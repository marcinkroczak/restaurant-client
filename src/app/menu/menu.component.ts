import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {error} from 'util';
import {ApiService} from '../shared/api.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  model: DishViewModel = {
    dishName: '',
    dishDescription: '',
    grossPrice: 0,
    cookingTime: 0,
  };
  constructor(private apiSevice: ApiService) {
  }

  ngOnInit() {
  }
  sendDish(): void {
    this.apiSevice.postDishes(this.model).subscribe(
      res => {
        location.reload();
      },
      err => {
        alert('error while sending dish');
      }
    );
  }
}

export interface DishViewModel {
  dishName: string;
  dishDescription: string;
  grossPrice: number;
  cookingTime: number;
}
