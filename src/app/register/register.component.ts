import { Component, OnInit } from '@angular/core';
import {ApiService} from '../shared/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model: UserViewModel = {
    username: '',
    password: '',
    email: '',
    phone: ''
  };
  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }
  sendUser(): void {
    this.apiService.postUser(this.model).subscribe(
      res => {
        location.reload();
      },
      err => {
        alert('error while adding user');
      }
    );
  }
}

export interface UserViewModel {
  username: string;
  password: string;
  email: string;
  phone: string;
}
