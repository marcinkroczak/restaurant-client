import { Component, OnInit } from '@angular/core';
import {ApiService} from '../shared/api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
model: LoginViewModel = {
  username: '',
  password: ''
};
  constructor(private apiService: ApiService, private router: Router) { }

  ngOnInit() {
  }
  sendLogin(): void {
    this.apiService.postLogin(this.model).subscribe(
      res => {
        // location.reload();
        console.log(res);
        localStorage.setItem('token', res.token);
        this.router.navigate(['/logged']);
      },
      err => {
        alert('error while login');
      }
    );
  }
}

export interface LoginViewModel {
  username: string;
  password: string;
}
