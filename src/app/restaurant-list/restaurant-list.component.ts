import { Component, OnInit } from '@angular/core';
import {Restaurant} from './model/restaurant';
import {ApiService} from '../shared/api.service';
import {Opinion} from '../opinions/model/opinion';
import {Table} from '../admin-panel/model/table';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.css']
})
export class RestaurantListComponent implements OnInit {
  restaurants: Restaurant[] = [];
  opinions: Opinion[] = [];
  tables: Table[] = [];
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getRestaurants();
    this.getOpinionById(0);
    this.getTablesById(0);
  }
  public getRestaurants() {
    this.apiService.getAllRestaurants().subscribe(
      res => {
        this.restaurants = res;
      },
      error => {
        alert('error has occured list restaurant');
      }
    );
  }
  public getOpinionById(id) {
    this.apiService.getRestaurantOpinionById(id).subscribe(
      res => {
        this.opinions = res;
      },
      error => {
        alert('error has occured list restaurant opinion');
      }
    );
  }
  public getTablesById(id) {
    this.apiService.getRestaurantTablesById(id).subscribe(
      res => {
        this.tables = res;
      },
      error => {
        alert('error has occured list restaurant opinion');
      }
    );
  }
}
