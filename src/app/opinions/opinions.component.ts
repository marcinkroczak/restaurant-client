import { Component, OnInit } from '@angular/core';
import {Opinion} from './model/opinion';
import {User} from '../admin-panel/model/user';
import {Restaurant} from '../restaurant-list/model/restaurant';
import {ApiService} from '../shared/api.service';

@Component({
  selector: 'app-opinions',
  templateUrl: './opinions.component.html',
  styleUrls: ['./opinions.component.css']
})
export class OpinionsComponent implements OnInit {
  opinions: Opinion[] = [];
  opinions2: Opinion[] = [];
  opinions3: Opinion[] = [];
  model: OpinionViewModel = {
    textOpinion: ''
  };
  resid = 1;
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getOpinionsRestaurant();
    this.getOpinionsRestaurant2();
    this.getOpinionsRestaurant3();
  }

  public getOpinionsRestaurant() {
    this.apiService.getOpinionsRestaurant(1).subscribe(
      res => {
        this.opinions = res;
      },
      error => {
        alert('error has occured list opinions');
      }
    );
  }
  public getOpinionsRestaurant2() {
    this.apiService.getOpinionsRestaurant(2).subscribe(
      res => {
        this.opinions2 = res;
      },
      error => {
        alert('error has occured list opinions');
      }
    );
  }
  public getOpinionsRestaurant3() {
    this.apiService.getOpinionsRestaurant(3).subscribe(
      res => {
        this.opinions3 = res;
      },
      error => {
        alert('error has occured list opinions');
      }
    );
  }
  sendOpinion(): void {
    this.apiService.postOpinion(this.model, 1, this.resid).subscribe(
      res => {
        location.reload();
      },
      err => {
        alert('error while sending opinion');
      }
    );
  }
}
export interface OpinionViewModel {
  textOpinion: string;
}
