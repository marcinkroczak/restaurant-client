import {Restaurant} from '../../restaurant-list/model/restaurant';

export class Opinion {
  opinionDate: string;
  opinionId: number;
  restaurant: Restaurant;
  textOpinion: string;
  userName: string;
}
