import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Dishes} from '../menu-show-dishes/model/dishes';
import {Restaurant} from '../restaurant-list/model/restaurant';
import {DishViewModel} from '../menu/menu.component';
import {TableViewModel} from '../admin-panel/admin-panel.component';
import {UserViewModel} from '../register/register.component';
import {LoginViewModel} from '../signin/signin.component';
import {Router} from '@angular/router';
import {Employee} from '../admin-panel/model/employee';
import {Order} from '../order/model/order';
import {EmployeeViewModel, RestaurantViewModel} from '../admin-panel/admin-panel.component';
import {User} from '../admin-panel/model/user';
import {DishUpdateModel} from '../menu-show-dishes/menu-show-dishes.component';
import {Opinion} from '../opinions/model/opinion';
import {ReservationViewModel} from '../reservation/reservation.component';
import {ReservationDTO} from '../reservation/model/reservation-dto';
import {OrderModel} from '../order/order.component';
import {OpinionViewModel} from '../opinions/opinions.component';
import {Table} from '../admin-panel/model/table';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private port = '90';
  private BASE_URL = 'http://localhost:' + this.port;
  private ALL_DISHES_URL = 'http://localhost:' + this.port + '/api/menu';
  private SEND_DISHES_URL = 'http://localhost:' + this.port + '/api/admin/menu';
  private SEND_USER_URL = 'http://localhost:' + this.port + '/api/users/signup';
  private SEND_LOGIN_URL = 'http://localhost:' + this.port + '/api/index/signin';
  private ALL_RESTAURANTS_URL = 'http://localhost:' + this.port + '/api/restaurants';
  private GET_EMPLOYEES_URL = 'http://localhost:' + this.port + '/api/admin/employees';
  private POST_EMPLOYEE_URL = 'http://localhost:' + this.port + '/api/admin/employees';
  private GET_USERS_URL = 'http://localhost:' + this.port + '/api/admin/users';
  private GET_USER_URL = 'http://localhost:' + this.port + '/index/users';
  private POST_RESTAURANT_URL = 'http://localhost:' + this.port + '/api/admin/restaurants';

  constructor(private http: HttpClient, private router: Router) {

  }

  getAllDishes(): Observable<Dishes[]> {
    return this.http.get<Dishes[]>(this.ALL_DISHES_URL);
}

  getAllRestaurants(): Observable<Restaurant[]> {
    return this.http.get<Restaurant[]>(this.ALL_RESTAURANTS_URL);
  }

  getAllOrders(): Observable<Order[]> {
    return this.http.get<Order[]>('http://localhost:' + this.port + '/api/orders');
  }

  getTables(): Observable<Table[]> {
    return this.http.get<Table[]>('http://localhost:' + this.port + '/api/tables');
  }

  getReservations(id): Observable<ReservationDTO[]> {
    return this.http.get<ReservationDTO[]>('http://localhost:' + this.port + '/api/users/' + id + '/reservations');
  }

  postDishes(dishes: DishViewModel): Observable<any> {
    return this.http.post(this.SEND_DISHES_URL, dishes);
  }

  postOpinion(opinions: OpinionViewModel, id, resid): Observable<any> {
    return this.http.post('http://localhost:' + this.port + '/api/users/' + id + '/opinions/' + resid, opinions);
  }

  postReservation(reservations: ReservationViewModel, id): Observable<any> {
    return this.http.post('http://localhost:' + this.port + '/api/users/' + id + '/reservations', reservations);
  }

  postOrder(orders: OrderModel): Observable<any> {
    return this.http.post('http://localhost:' + this.port + '/api/orders', orders);
  }

  postTable(tables: TableViewModel): Observable<any> {
    return this.http.post('http://localhost:' + this.port + '/api/admin/tables', tables);
  }

  updateDish(dishes: DishUpdateModel, id): Observable<any> {
    return this.http.put('http://localhost:' + this.port + '/api/admin/menu/' + id, dishes);
  }

  postUser(user: UserViewModel): Observable<any> {
    return this.http.post(this.SEND_USER_URL, user);
  }

  postLogin(login: LoginViewModel): Observable<any> {
    return this.http.post(this.SEND_LOGIN_URL, login);
  }

  // postEmployee(employee: EmployeeViewModel): Observable<any> {
  //  return this.http.post(this.GET_EMPLOYEES_URL, employee);
  // }

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.GET_EMPLOYEES_URL);
  }
  createEmployee(employee: EmployeeViewModel, id) {
    return this.http.post('http://localhost:' + this.port + '/api/admin/employees/' + id, employee);
  }

  deleteEmployee(id) {
    return this.http.delete('http://localhost:' + this.port + '/api/admin/employees/' + id);
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.GET_USERS_URL);
  }

  deleteUser(id) {
    return this.http.delete('http://localhost:' + this.port + '/api/users/' + id);
  }

  deleteReservation(id) {
    return this.http.delete('http://localhost:' + this.port + '/api/reservations/' + id);
  }

  addRestaurant(restaurant: RestaurantViewModel) {
    return this.http.post(this.POST_RESTAURANT_URL, restaurant);
  }

  getOpinionsRestaurant(id): Observable<Opinion[]> {
    return this.http.get<Opinion[]>('http://localhost:' + this.port + '/api/restaurants/' + id + '/opinions');
  }

  getRestaurantOpinionById(id): Observable<Opinion[]> {
    return this.http.get<Opinion[]>('http://localhost:' + this.port + '/api/restaurants/' + id + '/opinions');
  }

  getRestaurantTablesById(id): Observable<Table[]> {
    return this.http.get<Table[]>('http://localhost:' + this.port + '/api/restaurants/' + id + '/tables');
  }

  getOrder(id): Observable<Order> {
    return this.http.get<Order>('http://localhost:' + this.port + '/api/orders/' + id);
  }

  // getUserById(): Observable<OneUser> {
  //  return this.http.get<OneUser>(this.GET_USER_URL);
  // }

  loggedIn() {
    return !!localStorage.getItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }

  logoutUser() {
    localStorage.removeItem('token');
    this.router.navigate(['/signin']);
  }
}
