export class ReservationDTO {
  reservationId: number;
  comments: string;
  dateReservation: string;
  tableId: number;
}
