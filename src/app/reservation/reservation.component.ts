import { Component, OnInit } from '@angular/core';
import {ApiService} from '../shared/api.service';
import {ReservationDTO} from './model/reservation-dto';
import {Restaurant} from '../restaurant-list/model/restaurant';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {
  reservations: ReservationDTO[] = [];
  model: ReservationViewModel = {
    comments: 'aaa',
    dateReservation: '2020-01-28T14:33:38.054Z',
    tableId: 1
  };

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getReservation();
  }
  public getReservation() {
    this.apiService.getReservations(1).subscribe(
      res => {
        this.reservations = res;
      },
      error => {
        alert('error has occured list reservation');
      }
    );
  }
  sendReservation(): void {
    this.apiService.postReservation(this.model, 1).subscribe(
      res => {
        location.reload();
      },
      err => {
        alert('error while sending reservation');
      }
    );
  }
  public deleteReservation(id) {
    this.apiService.deleteReservation(id).subscribe(
      res => {
        location.reload();
      },
      error => {
        alert('error has occured delete reservation');
      }
    );
  }
}
export interface ReservationViewModel {
  comments: string;
  dateReservation: string;
  tableId: number;
}
