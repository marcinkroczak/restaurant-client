import { Component, OnInit } from '@angular/core';
import {Employee} from './model/employee';
import {Table} from './model/table';
import {ApiService} from '../shared/api.service';
import {User} from './model/user';
import {DishViewModel} from '../menu/menu.component';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {
  constructor(private apiService: ApiService) { }
employees: Employee[] = [];
users: User[] = [];
tables: Table[] = [];
model: EmployeeViewModel;
  modelRes: RestaurantViewModel = {
    city: '',
    street: ''
  };
  modelTable: TableViewModel = {
    seatsNumber: 1,
    restaurantId: 1
  };
  modelEmp: EmployeeViewModel = {
    name: 'f',
    surname: '',
    roles: ['WAITER'],
    salary: 0
  };
  empid = 0;
  public details = false;

  ngOnInit() {
    this.getEmployees();
    this.getAllUsers();
    this.getTables();
  }
  public getEmployees() {
    this.apiService.getEmployees().subscribe(
      res => {
        this.employees = res;
      },
      error => {
        alert('error has occured list employees');
      }
    );
  }

  public deleteEmployee(id) {
    this.apiService.deleteEmployee(id).subscribe(
      res => {
        location.reload();
      },
      error => {
        alert('error has occured delete emplyee');
      }
    );
  }

  createEmployee(): void {
    this.apiService.createEmployee(this.modelEmp, this.empid).subscribe(
      res => {
        location.reload();
      },
      err => {
        alert('error while creating employee');
      }
    );
  }

  public getAllUsers() {
    this.apiService.getAllUsers().subscribe(
      res => {
        this.users = res;
      },
      error => {
        alert('error has occured list users');
      }
    );
  }
  public userEdit(id) {

  }

  public deleteUser(id) {
    this.apiService.deleteUser(id).subscribe(
      res => {
        location.reload();
      },
      error => {
        alert('error has occured delete user');
      }
    );
  }

  addRestaurant(): void {
    this.apiService.addRestaurant(this.modelRes).subscribe(
      res => {
        location.reload();
      },
      err => {
        alert('error while adding restaurant');
      }
    );
  }
  sendTable(): void {
    this.apiService.postTable(this.modelTable).subscribe(
      res => {
        location.reload();
      },
      err => {
        alert('error while sending table');
      }
    );
  }
  public getTables() {
    this.apiService.getTables().subscribe(
      res => {
        this.tables = res;
      },
      error => {
        alert('error has occured list tables');
      }
    );
  }
}


export interface EmployeeViewModel {
  name: string;
  surname: string;
  roles: string[];
  salary: number;
}

export interface RestaurantViewModel {
  city: string;
  street: string;
}

export interface TableViewModel {
  seatsNumber: number;
  restaurantId: number;
}
