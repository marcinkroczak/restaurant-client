export class Employee {
  employeeId: number;
  userId: number;
  name: string;
  surname: string;
  salary: number;
}
