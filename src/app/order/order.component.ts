import { Component, OnInit } from '@angular/core';
import {Dishes} from '../menu-show-dishes/model/dishes';
import {ApiService} from '../shared/api.service';
import {Order} from './model/order';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  constructor(private apiService: ApiService) { }
  dishes: Dishes[] = [];
  orders: Order[] = [];
  order1: Order;
  model: OrderModel = {
    reservationId: 1,
    employeeId: 1,
    customerId: 1,
    dishesIds: [1, 2],
    additionalRemarks: 'aaa'
};
  i = 0;

  ngOnInit() {
    this.getDishes();
    this.getOrders();
  }
  public getDishes() {
    this.apiService.getAllDishes().subscribe(
      res => {
        this.dishes = res;
      },
      error => {
        alert('error has occured list dishes');
      }
    );
  }
  public addToOrder(id) {
    this.model.dishesIds[this.i] = id;
    this.i = this.i + 1;
  }
  sendOrder(): void {
    this.apiService.postOrder(this.model).subscribe(
      res => {
        location.reload();
      },
      err => {
        alert('error while sending order');
      }
    );
  }
  public getOrders() {
    this.apiService.getAllOrders().subscribe(
      res => {
        this.orders = res;
      },
      error => {
        alert('error has occured list orders');
      }
    );
  }
  public getOrderById(id) {
    this.apiService.getOrder(id).subscribe(
      res => {
        this.order1 = res;
      },
      error => {
        alert('error has occured list order');
      }
    );
  }
}

export interface OrderModel {
  reservationId: number;
  employeeId: number;
  customerId: number;
  dishesIds: number[];
  additionalRemarks: string;
}
