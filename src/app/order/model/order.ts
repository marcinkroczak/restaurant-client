import {Dishes} from '../../menu-show-dishes/model/dishes';

export class Order {
  additionalRemarks: string;
  bill: number;
  employeeId: number;
  orderDate: string;
  orderId: number;
  orderedDishes: Dishes;
  reservationId: number;
  status: string;
}
