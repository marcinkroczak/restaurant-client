import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MenuComponent} from './menu/menu.component';
import {RestaurantListComponent} from './restaurant-list/restaurant-list.component';
import {OpinionsComponent} from './opinions/opinions.component';
import {NotFoundComponentComponent} from './not-found-component/not-found-component.component';
import {config} from 'rxjs';
import {IndexPageComponent} from './index-page/index-page.component';
import {SigninComponent} from './signin/signin.component';
import {RegisterComponent} from './register/register.component';
import {LoggedComponent} from './logged/logged.component';
import {AuthGuard} from './auth.guard';
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {ReservationComponent} from './reservation/reservation.component';
import {OrderComponent} from './order/order.component';


const routes: Routes = [
  {
    path: '',
    component: IndexPageComponent
  },
  {
    path: 'admin',
    component: AdminPanelComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'logged',
    component: LoggedComponent,
    // canActivate: [AuthGuard]
  },
  {
    path: 'menu',
    component: MenuComponent
  },
  {
    path: 'restaurant-list',
    component: RestaurantListComponent
  },
  {
    path: 'opinions',
    component: OpinionsComponent
  },
  {
    path: 'signin',
    component: SigninComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'reservation',
    component: ReservationComponent
  },
  {
    path: 'order',
    component: OrderComponent
  },
  {
    path: '**',
    component: NotFoundComponentComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
