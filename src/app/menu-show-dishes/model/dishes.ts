export class Dishes {
  dishId: number;
  dishName: string;
  dishDescription: string;
  grossPrice: number;
  cookingTime: number;
}
