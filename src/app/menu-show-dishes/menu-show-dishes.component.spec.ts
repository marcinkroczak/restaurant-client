import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuShowDishesComponent } from './menu-show-dishes.component';

describe('MenuShowDishesComponent', () => {
  let component: MenuShowDishesComponent;
  let fixture: ComponentFixture<MenuShowDishesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuShowDishesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuShowDishesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
