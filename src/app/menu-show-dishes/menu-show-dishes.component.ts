import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Dishes} from './model/dishes';
import {ApiService} from '../shared/api.service';
import {DishViewModel} from '../menu/menu.component';

@Component({
  selector: 'app-menu-show-dishes',
  templateUrl: './menu-show-dishes.component.html',
  styleUrls: ['./menu-show-dishes.component.css']
})
export class MenuShowDishesComponent implements OnInit {
  dishes: Dishes[] = [];
  upmodel: DishUpdateModel = {
    dishName: '',
    dishDescription: '',
    grossPrice: 0,
    cookingTime: 0,
  };
  dishId: number;
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getDishes();
  }
  public getDishes() {
    this.apiService.getAllDishes().subscribe(
      res => {
        this.dishes = res;
      },
      error => {
        alert('error has occured list dishes');
      }
    );
  }

  updateDish(): void {
    this.apiService.updateDish(this.upmodel, this.dishId).subscribe(
      res => {
        location.reload();
      },
      err => {
        alert('error while updating dish');
      }
    );
  }
  }

export interface DishUpdateModel {
  dishName: string;
  dishDescription: string;
  grossPrice: number;
  cookingTime: number;
}

