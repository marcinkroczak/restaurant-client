import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { MenuComponent } from './menu/menu.component';
import { RestaurantListComponent } from './restaurant-list/restaurant-list.component';
import { OpinionsComponent } from './opinions/opinions.component';
import { NotFoundComponentComponent } from './not-found-component/not-found-component.component';
import {FormsModule} from '@angular/forms';
import { SigninComponent } from './signin/signin.component';
import { RegisterComponent } from './register/register.component';
import { IndexPageComponent } from './index-page/index-page.component';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { MenuShowDishesComponent } from './menu-show-dishes/menu-show-dishes.component';
import { LoggedComponent } from './logged/logged.component';
import {AuthGuard} from './auth.guard';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import {TokenInterceptorService} from './shared/token-interceptor.service';
import { ReservationComponent } from './reservation/reservation.component';
import { OrderComponent } from './order/order.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    MenuComponent,
    RestaurantListComponent,
    OpinionsComponent,
    NotFoundComponentComponent,
    SigninComponent,
    RegisterComponent,
    IndexPageComponent,
    MenuShowDishesComponent,
    LoggedComponent,
    AdminPanelComponent,
    ReservationComponent,
    OrderComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
